package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

//Article is representation of an article
type Article struct {
	Title   string `json:"Title"`
	Desc    string `json:"desc"`
	Content string `json:"content"`
}

//Articles is representation of an article
type Articles []Article

func getTree(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the HomePage!")

}

func deleteNode(w http.ResponseWriter, r *http.Request) {

}

func addNode(w http.ResponseWriter, r *http.Request) {

}

func handleRequests() {

	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", getTree).Methods("GET")
	myRouter.HandleFunc("/add/{parent}", addNode).Methods("POST")
	myRouter.HandleFunc("/delete/{node}", deleteNode).Methods("DELETE")
	log.Fatal(http.ListenAndServe(":10061", myRouter))
}

func main() {
	handleRequests()
}
